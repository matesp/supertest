const chai = require('chai')
, assert = chai.assert;
var request = require('supertest')('https://randomuser.me/');

const amountOfResults = 50;


describe('GET /api/', function() {
    it('should responds with json', function(done) {
      request
        .get('/api')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it('password length should be between 1-16 chars', function(done) {
        request
          .get('/api')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200, done)
          .expect(response => {
            var count = Object.keys(response.body.results[0].login.password).length
            assert.isAtLeast(count, 1)
            assert.isAtMost(count, 16)
          });
      });
  });

describe('GET /api/?gender=male', function(){
    it('should return a male user', function(done) {
        request
          .get('/api/?gender=male')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200, done)
          .expect(response => {
              assert.equal(response.body.results[0].gender, "male")
          });
      });
})

describe('GET /api/nonexistinguser', function(){
    it('response should be 404 not found', function(done) {
        request
          .get('/api/nonexistinguser')
          .expect(404, done)
      });
})

describe('GET /api/?results=' + amountOfResults, function(){
    it('should return a certain amount of results', function(done) {
        request
          .get('/api/?results=' + amountOfResults)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200, done)
          .expect(response => {
              var count = response.body.results.length
              assert.equal(count, amountOfResults)
          });
      });
})

describe('GET /api/?nat=us&randomapi', function(){
    it('should return a user with US nationality', function(done) {
        request
          .get('/api/?nat=us&randomapi')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200, done)
          .expect(response => {
              assert.equal(response.body.results[0].nat, 'US')
          });
      });
})



